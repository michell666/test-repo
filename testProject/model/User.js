//Database model
const mongoose = require('mongoose');

//Users collection
const userSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true,
        max: 255,
        min: 2
    },
    email:{
        type: String,
        required: true,
        max: 255,
        min: 6
    },
    password:{
        type: String,
        required: true,
        max: 255,
        min: 6

    },
    token:{
        type: String

    }
});

module.exports = mongoose.model('User',userSchema);
const express = require('express');
const app = express();
const dotenv = require('dotenv');
const mongoose = require('mongoose');

//Import Routes
const registerRoute = require('./routes/register');
const loginRoute = require('./routes/login');
const listCryptoCurrencyRoute = require('./routes/listCryptoCurrency');


dotenv.config();


//Connect to Database
mongoose.connect(process.env.DB_CONNECT,
{useNewUrlParser:true},
() => console.log('connected to db!') );

//Middleware
app.use(function (req, res, next) {

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'auth-token');

    // Pass to next layer of middleware
    next();
});
app.use(express.json());

app.get('/',(req,res)=> {
    res.send("Hello");})

//Route Middlewares
app.use('/api/user',registerRoute);
app.use('/api/user',loginRoute);
app.use('/api',listCryptoCurrencyRoute);




const PORT = process.env.PORT || 3000
app.listen(3000,() => console.log("Server Up and Running!"));
const router = require('express').Router();
const User = require('../model/User');
const {loginValidation} = require('../validation');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


//Login
router.post('/login', async (req,res) => {
    //Validate data
    const {error} = loginValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    //Check if the email exist
    const user = await User.findOne({email : req.body.email});
    if (!user) return res.status(400).send("Email doesn't exist");

    //Check if password correct
    const validPass = await bcrypt.compare(req.body.password,user.password);
    if (!validPass) return res.status(400).send("Password is not correct!");

    //Get access token
    const token  =user.token
    
    //Set access token to the header with title "auth-token"
    res.setHeader('auth-token',token);

    
    //Return user details in json
    res.json({Details : {name : user.name , email: user.email}});



});
module.exports = router;

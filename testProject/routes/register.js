const router = require('express').Router();
const User = require('../model/User');
const {registerValidation} = require('../validation');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');



router.post('/register', async (req,res) =>{
    //Validate data
    const {error} = registerValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    //Check if the user already exist
    const emailExist = await User.findOne({email : req.body.email});
    if (emailExist) return res.status(400).send('Email already exist!');

    //Hash the password
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password,salt);

    //Create new user
    const user = new User({
        name : req.body.name,
        email : req.body.email,
        password : hashPassword
    });

    try{
        //Store the user
        const registeredUser = await user.save();

    }catch(err){
        res.status(400).send(err);
    }

    //Generate access token using user id and token secret
    const userData = await User.findOne({email : req.body.email});
    const token = jwt.sign({_id: userData._id},process.env.TOKEN_SECRET);

   
    try{
         //Store access token
        const updated = await User.findOneAndUpdate({email : req.body.email},{token: token},{returnOriginal:false});
        
        // //Return user details
        // const details =  await User.find({_id: userData._id},{'_id':0,'name':1,'email':1});
        // res.send(details);

    }catch(err){
        res.status(400).send(err);
    }

    //Return user details in json
    res.json({details : {name : req.body.name,email : req.body.email}});

});

module.exports = router;
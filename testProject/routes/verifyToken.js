//Verify access token
const jwt = require('jsonwebtoken');

module.exports = function (req,res,next) {
    //Check if there is any access token set to the header
    const token = req.header('auth-token');
    if(!token) return res.status(401).json({Access :  "Denied"});

    try{
        //Verify if the token valid then return payload with user id
        const verified = jwt.verify(token,process.env.TOKEN_SECRET);
        req.user = verified;
        next();
    }catch(err){
        res.status(400).json({Result : "Invalid Token"});
    }
}

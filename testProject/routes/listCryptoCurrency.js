const router = require('express').Router();
const verify = require('./verifyToken');
const axios = require('axios');
//const CoinMarketCap = require('coinmarketcap-api');

 
// const apiKey = 'e7ad74d4-7324-4c8c-89ed-8ec0777210e6';
// const client = new CoinMarketCap(apiKey);





//Verify access token then Return Crypto Currency List
router.get('/currency', verify, async (req,res) =>{

    //To get list of latest crypto currency via coinmarketcap API
   const dt = await axios({
        method : 'GET',
        url:'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest',
        headers: {
            'X-CMC_PRO_API_KEY': process.env.CRYPTO_API_KEY
          }
        });

    //Return the crypto currency data in json
    res.json((dt.data).data);

    
    // client.getTickers().then((d) =>
    // {
    //    res.send(JSON.parse(JSON.stringify(d.data)));
    // } ).catch(console.error);

})

module.exports  = router;
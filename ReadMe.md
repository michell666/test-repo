INTRODUCTION
----------------------

This API application consists of THREE API :

* Register : Require user details input, generate access token and store all data in the database.Return basic user detail.

* Login:	Require login input, set access token to header and return the user data after logged in.

* ListCryptoCurrency: Verify access token then return list of crypto currency.



INSTALLATION
----------------------

1. Install Nodejs


2. Clone the repo

	git clone https://michell666@bitbucket.org/michell666/test-repo.git
	
	
3. Install NPM packages

* Express:		npm i -s express

* Nodemon:		npm install --save-dev nodemon

* Mongoose: 	npm i -s mongoose

* Dotenv:		npm i -s dotenv

* Joi:			npm i -s joi

* Bcrypt:		npm i -s bcrypt

* JsonWebToken: npm i -s jsonwebtoken

* Axios:		npm i -s axios


4. Create MongoDB atlas database at https://www.mongodb.com/cloud/atlas/register


5. Get connection string from your new database and store it in .env file

	e.g : 
	
		DB_CONNECT = mongodb+srv://testing123:testing123@cluster0.zvktp.mongodb.net/myFirstDatabase?retryWrites=true&w=majority


6. In package.json , update "scripts" to
	
	"scripts": {
    "start": "nodemon index.js"}
	
  
7. Create a token secret in .env , it can be any string

	e.g :
	
		TOKEN_SECRET = efsdfsadfasfdfsdg
		

8. Get coinmarketcap API at https://coinmarketcap.com/api/documentation/v1/


9. Store coinmarketcap API at .env

	e.g 
	
		CRYPTO_API_KEY = e7ad74d4-7324-4c8c-89ed-8ec0777210e6
	

10. Install Postman for API testing at https://www.postman.com



USER GUIDE
----------------------

1. Open the ‘testProject’ folder in Visual Studio Code

2. Open terminal and input “npm   start” to run the server 

[NPM START Reference](Images/Ref2.png)

3. Open Postman application to create request to test the API 

* Register API:

		Method: POST
		
		Url : http://localhost:3000/api/user/register
		
		Input/Body ( raw-JSON ):
		
		{
		
			“name” : [name],
			
			“email” : [email],
			
			“password” : [password]
			
		}
		

--The API will validate the input, set access token, store all details  then return some basic details of the registered user

[Register Reference](Images/Ref3.png)

--Refresh the database and you will notice the user information and access token are stored in the users database

[Data Stored Reference](Images/Ref4.png)



* Login API:

		Method : POST
		
		Url : http://localhost:3000/api/user/register 
		
		Input/Body(raw-JSON):
		
		{
		
			“email” : [Login Email]
			
			“password” : [Password]
			

		}

--The API will validate and verify the inputs, get the access token and set it to header with title “auth-token”.

[Login API Reference](Images/Ref5.png)
	
--When the user has login successfully, the access token belong to the user will be store in the request header (“auth-token”) for access.

[Access Token Stored Reference](Images/Ref6.png)


* ListCryptoCurrency API:

		Method: GET
		
		Url: http://localhost:3000/api/currency
		
		Header: Set “auth-token” with 
		
			
			1.No access Token -- Unable to access the data 
[No Access Code Reference](Images/Ref7.png)

			2.Valid access code -- A list of latest crypto currency data shown
[Valid Access Code Reference](Images/Ref8.png)




ADDITIONAL DETAILS
----------------------
**Project directory


[Project Directory Reference](Images/Ref9.png)



* Model folder : 

	User.js : Create the user collection schema

* Routes folder :
	
	listCryptoCurrency.js : listCryptoCurrency API

	login.js : login API

	register.js : register API

	verifyToken.js : Verify User Access Token

* .env :  Store environment variables such as database connection string, API endpoints, token secret, Coinmarkeycap api key

* Index.js : connect database, middleware, connect route and create serve
	
* Validation.js : input validation


**Database(MongoDB Atlas )


[Database Reference](Images/Ref10.png)


Our database has one collection : users

		_id : user id 
		Name : user name
		Email : user email
		Password : user password (hashed)
		Token : access token

FLOWCHART
----------------------


Register

[Register Flowchart](Images/Ref11.png)


Login

[Login Flowchart](Images/Login.png)


List Crypto Currency

[List Crypto Currency Flowchart](Images/CryptoCurrency.png)